Drupal.behaviors.ucAttribsByStock = function (context) {
	Drupal.ucAttribsByStock = {}
	Drupal.ucAttribsByStock.$atcForm = $('form[id^="uc-product-add-to-cart-form"]');
	Drupal.ucAttribsByStock.$attributes = Drupal.ucAttribsByStock.$atcForm.find('div.attributes select');
	Drupal.ucAttribsByStock.combo = {};
	var i = 0;
	Drupal.ucAttribsByStock.$attributes.each(function(){
		var id = $(this).attr('id').split('-');
		$(this).data('aid', id[id.length - 1]);
		if(Drupal.ucAttribsByStock.$attributes[i+1]){
			$(this).data('next-attrib', Drupal.ucAttribsByStock.$attributes[i+1]);
		}
		if(Drupal.ucAttribsByStock.$attributes[i-1]){
			$(this).data('prev-attrib', Drupal.ucAttribsByStock.$attributes[i-1]);
		}
		$(this).bind('update-options', function(e){
			var path = Drupal.settings.uc_attribs_by_stock.page_callback + '/' 
				+ Drupal.settings.uc_attribs_by_stock.nid + '/'
				+ $(this).data('aid') + '/'
				+ JSON.stringify(Drupal.ucAttribsByStock.combo);
			var $attribute = $(this);
			$.getJSON(encodeURI(path), function(data){
				var attribData = data;
				var newOptions = '<option selected="selected" value="">Please select</option>';
				for(i in attribData.options){
					newOptions += '<option value="' + attribData.options[i].oid + '">' + attribData.options[i].name + '</option>';
				}
				$attribute.html($(newOptions)).removeAttr('disabled');
			});
		});
		$(this).bind('reset-options', function(e){
			$(this).html('<option selected="selected" value="">Please select</option>');
			$(this).attr('disabled', 'disabled');
		});
		$(this).change(function(e){
			if($(this).data('next-attrib')){
				var $nextAttrib = $($(this).data('next-attrib'));
				if($(this).val()){
					Drupal.ucAttribsByStock.combo[$(this).data('aid')] = $(this).val();
					$nextAttrib.trigger('update-options');
				} else if(!$(this).val()){
					do{
						delete Drupal.ucAttribsByStock.combo[$nextAttrib.data('aid')];
						$nextAttrib.trigger('reset-options');
						$nextAttrib = $($nextAttrib.data('next-attrib'));
					} while($nextAttrib.data('next-addrib'));
				}	
			}
			
		});
		i++;
	});
}


